gamma = ureal('gamma',2,'Perc',30);  % uncertain gain

tau = ureal('tau',1,'Perc',30);      % uncertain time-constant

% parameters of second order mode
wn = 50; xi = 0.25;

% uncertain plant
P = tf(gamma,[tau 1]) * tf(wn^2,[1 2*xi*wn wn^2]);

% Add unmodeled dynamics
delta = ultidyn('delta',[1 1],'SampleStateDim',5,'Bound',1);
W = makeweight(0.1,20,10);
P = P * (1+W*delta);

figure(1),step(P,5)

figure(2),bode(P)

Parray = usample(P,60);

Pn = P.NominalValue;

Wt=tf(0.4*[1 1],[1/60 1]);

figure(3), bodemag((Pn-Parray)/Pn,Wt,'r')

