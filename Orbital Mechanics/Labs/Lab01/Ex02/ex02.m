% Gravitational parameter
mu = 398600.44;

% Let us choose a,e as the two D.o.F of the 2D orbit path (along w/ theta for defining the S/C position)
a = 7323;
e = .393;

p = a*(1-e^2);
h = sqrt(mu*p);
r_p = (h^2/mu) * 1/(1+e);
v_p = (mu/h)*(1+e);

% Initial conditions
rr_0 = [r_p 0 0]';
vv_0 = [0 v_p 0]';

y0 = [rr_0; vv_0];

% Set options
options = odeset( 'RelTol', 1e-13, 'AbsTol', 1e-14 );

% Set time span
T = 2*pi*sqrt(a^3/mu);
tspan = [0:100:T];

% Perform the integration
[T, Y] = ode113( @(t,y) ode_2body(t,y, mu), tspan, y0, options );

% Plot NUMERICAL solution
figure
subplot(1,2,1)
plot(T, Y(:,1), '-o');
xlabel('Time [s]');
ylabel('Position [m]');

subplot(1,2,2)
plot(T, Y(:,2), '-o');
xlabel('Time [s]');
ylabel('Velocity [m/s]');