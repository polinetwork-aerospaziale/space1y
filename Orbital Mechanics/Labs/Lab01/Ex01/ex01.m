% Oscillator parameters
omega0 = 10;
gamma = .1;

% Initial conditions
y0 = [.1    0];

% Set options
options = odeset( 'RelTol', 1e-13, 'AbsTol', 1e-14 );

% Set time span
tspan = [0:.01:10];

% Perform the integration
[T, Y] = ode113( @(t,y) ode_harmonic_oscill(t,y, omega0, gamma), tspan, y0, options );

% Plot NUMERICAL solution
figure
subplot(1,2,1)
plot(T, Y(:,1), '-o');
xlabel('Time [s]');
ylabel('Position [m]');

subplot(1,2,2)
plot(T, Y(:,2), '-o');
xlabel('Time [s]');
ylabel('Velocity [m/s]');




% Plot ANALYTICAL solution
tspan = [0:.001:10];
[T, Y] = exactSol_harmonic_oscill(tspan, y0, omega0, gamma)

subplot(1,2,1)
hold on
plot(T, Y(:,1), 'r');
xlabel('Time [s]');
ylabel('Position [m]');

subplot(1,2,2)
hold on
plot(T, Y(:,2), 'v');
xlabel('Time [s]');
ylabel('Velocity [m/s]');