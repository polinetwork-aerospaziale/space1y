%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       ORBITAL MECHANICS                                 %
%                    Academic year 2018/2019                              %
%                                                                         %
%                    Lab 3: Lambert's problem                             %
%                          24/10/2018                                     %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% DESCRIPTION: Sample script illustating the use of lambertMR
%
% Camilla Colombo, 11/11/2016
%

clc;
clear;

% % Note: read Help of lambertMR
% % lambertMR(RI,RF,TOF,MU,orbitType,Nrev,Ncase,optionsLMR)
% % as input you need only:
% % RI,RF,TOF,MU,
% % for the other parameters set:
% % orbitType = 0;
% % Nrev = 0;
% % optionsLMR = 0;
% 
mu = 398600.44;      % Gravitational parameter [km^3/s^2];



RR1 = [-21800, 37900, 0 ];
RR2 = [27300, 27700, 0];
ToF = 54400;                 % Time of flight [s];

[a,p,e,ERROR,VV1,VV2,TPAR,theta] = lambertMR( RR1, RR2 , ToF, mu, 0, 0, 2 );
RR1 = RR1(:); RR2 = RR2(:); VV1 = VV1(:); VV2 = VV2(:);



y0 = [RR1; VV1];

% Set options
options = odeset( 'RelTol', 1e-13, 'AbsTol', 1e-14 );

% Set time span
tspan = [0:100:ToF];

% Perform the integration
[T, StateMat] = ode113( @(t,y) ode_2body(t,y, mu), tspan, y0, options )

X = StateMat(:,1); Y = StateMat(:,2); Z = StateMat(:,3);
VX = StateMat(:,4); VY = StateMat(:,5); VZ = StateMat(:,6);



figure
plot3(X,Y,Z)
grid on















