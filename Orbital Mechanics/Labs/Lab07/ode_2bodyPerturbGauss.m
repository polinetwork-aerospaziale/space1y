function dy = ode_2bodyPerturbGauss( t, y, mu )

%ode_2body ODE system for the perturbed two-body problem %
% PROTOTYPE
% dy = ode_2bodyPerturb( t, y, mu )
%
% INPUT:
% t[1]          Time 
% y[6x1]        State of the oscillator:
%               elems. 1:3 -> position vector [km]
%               elems. 4:6 -> velocity vector [km/s]
%   
% mu[1]         Gravitational parameter [km^3/s^2]
%
% OUTPUT:
%   dy[6x1]
%
% CONTRIBUTORS:
%   Massimo Piazza
%
% VERSIONS
%   2018-11-04: 1.0
%

a  = y(1);   e  = y(2);   i  = y(3);
OM = y(4);   om = y(5);   th = y(6);

[rr, vv] = kep2car(a,e,i,OM,om,th, mu);

r = norm(rr);
v = norm(vv);
x = rr(1);
y = rr(2);
z = rr(3);


% J2 perturbing acceleration in cartesian coordinates:
R_e = 6378.137;
J2 = 0.00108263;
kJ2 = 1.5*J2*mu*R_e^2/r^4;
a_J2_x = kJ2 * x/r*(5*z^2/r^2-1);
a_J2_y = kJ2 * y/r*(5*z^2/r^2-1);
a_J2_z = kJ2 * z/r*(5*z^2/r^2-3);

a_J2_xyz = [a_J2_x a_J2_y a_J2_z]';



% J2 perturbing acceleration in {t,n,h} coordinates:
tt = vv/norm(vv);                 % Tangent  unit vector
hh = cross(rr,vv); hh = norm(hh); % Normal   unit vector
nn = cross(hh,tt);                % Binormal unit vector
ROT_tnh2xyz = [tt(:) nn(:) hh(:)];

a_J2_tnh = ROT_tnh2xyz' * a_J2_xyz;



% Variation of orbital elements:
b = a * sqrt(1-e^2);
p = b^2/a;
% r = p / (1+e*cos(th));
% v = sqrt(2*mu/r - mu/a);
n = sqrt(mu/a^3);
h = n*a*b;
th_star = th + om;

a_dot  = 2*a^2*v/mu * a_t;
e_dot  = 1/v* ( 2*(e+cos(th))*a_t - r/a*sin(th)*a_n );
i_dot  = r*cos(th_star)/h * a_h;
OM_dot = r*sin(th_star)/(h*sin(i)) * a_h;
om_dot = 1/(e*v) * ( 2*sin(th)*a_t + (2*e + r/a*cos(th))*a_n ) - r*sin(th_star)*cos(i)/(h*sin(i))*a_h;
th_dot = h/r^2 - 1/(e*v) * ( 2*sin(th)*a_t + (2*e + r/a*cos(th))*a_n );


% Set the derivatives of the state
dy = [  a_dot ;
        e_dot ;
        i_dot ;
        OM_dot;
        om_dot;
        th_dot];
 
end

