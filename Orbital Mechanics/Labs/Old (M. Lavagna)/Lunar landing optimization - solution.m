function [TIME, Y, ecc, sma, EE, THE, R_orb] = landing_solution
close all
orig_state = warning;

%TARGET
uf =  0; %Target horizontal speed [m/s]
vf = -3; %Target vertical speed [m/s]
hf = 30; %Target altitude [m]

%LANDER
Isp = 320; %Specific impulse [s]
T   = 2500; %Thrust [N]
m0  = 1600; %Initial mass [kg]

%PHYSICAL CONSTANTS
g0      = 9.80665;    %Standard gravity [m/s^2]
mu_moon = 4.9037e+12; %Moon gravity constant [N*m^2/kg]
r_moon  = 1737.5e3;   %Moon mean radius [m]

%TRANSFER ORBIT
ha = 100e3;         %Apocenter altitude (aka Parking Orbit altitude) [m]
hp = 10e3;          %Pericenter altitude [m]
ra = r_moon + ha;   %Apocenter radius [m]
rp = r_moon + hp;   %Pericenter radius [m]

%INITIAL CONDITIONS
rho0   = rp;                                    %Rho
theta0 = 0;                                     %Theta
v0     = 0;                                     %Vertical speed
u0     = sqrt( 2*mu_moon*( 1/rp - 1/(rp+ra) ) );%Horizontal speed
lambdarho0 = 1;

%NUMERICAL INTEGRATION OPTIONS
maxtime = 1500;                                     %Maximum landing time [s]
odeopt  = odeset('Events',@events,'RelTol',1e-5);   %ode45 options


% --------------------------- MULTISTART OPTIMIZATION --------------------------

randomBound = 1e3;  %Bound for random initial points
randomN = 50;       %Number of random pointo to be generated
ptmatrix = 2*randomBound*(rand(randomN,2)-0.5); %Custom initial points
stpoints = CustomStartPointSet(ptmatrix);       %Starting points object creation

%fminunc (local optimizer) options
fminopt = optimoptions(@fminunc,...
                       'Display', 'none',...
                       'TolFun', 1e-6,...
                       'TolX', 1e-9,...
                       'Algorithm', 'quasi-newton',...
                       'maxFunEvals', 500);
%Optimizazion problem
problem = createOptimProblem('fminunc',...
                             'objective',@(x)objective(x),...
                             'x0', [0.01, -0.01],...
                             'options', fminopt);
%Multistart object
ms = MultiStart('Display','iter');
            
x = run(ms,problem,stpoints);   %SOLVE OPTIMIZATION


% --------------------------------- VIEW RESULTS -------------------------------

%Final Simulation
Y0 = [rho0 theta0 v0 u0 lambdarho0 x(1) x(2)]'; %Initial conditions vector
[TIME,Y] = ode45(@landingSystem,[0 maxtime],Y0,odeopt); %Numerical integration

%Transfer Orbit Computation
ecc = (ra-rp)/(ra+rp);  %Eccentricity
sma = 0.5*(ra+rp);      %Semi-major Axis
EE = zeros(size(TIME)); %Mean Anomaly preallocation
fsolopt = optimoptions('fsolve', 'display', 'off'); %fsolve options
for ii = 1:length(EE)   %Mean Anomaly computation
    EE(ii) = fsolve(@(E)E-ecc*sin(E)-TIME(ii)*sqrt(mu_moon/sma^3), 0.5, fsolopt);
end
THE = acos( (cos(EE)-ecc)./(1-ecc*cos(EE)) );   %True Anomaly
R_orb = sma*(1-ecc^2)./(1+ecc*cos(THE));        %Radius


%Plottings

figure(1) %Altitude (rho-r_moon)
hold on
plot(TIME,Y(:,1)-r_moon);
plot(TIME,R_orb-r_moon, 'r', 'linewidth', 1);
xlabel('Time [s]');
ylabel('Altitude [m]');
legend('Landing trajectory', 'Transfer Orbit', 'location', 'best');
grid on

figure(2) %Angle (theta)
plot(TIME,Y(:,2));
xlabel('Time [s]');
ylabel('Theta [rad]');
grid on

figure(3) %Vertical speed (v)
plot(TIME,Y(:,3));
xlabel('Time [s]');
ylabel('Vertical speed [m/s]');
grid on

figure(4) %Horizontal speed (u)
plot(TIME,Y(:,4));
xlabel('Time [s]');
ylabel('Horizontal speed [m/s]');
grid on

figure(5) %Thrust angle (beta)
plot(TIME,(atan(Y(:,6)./Y(:,7))+pi)*180/pi);
xlabel('Time [s]');
ylabel('Thrust Angle [deg]');
grid on

figure(6) %Trajectory plot
hold on
polar(Y(:,2)+pi/2, Y(:,1), 'b');
polar(Y(:,2)+pi/2, r_moon*ones(size(Y(:,2))), 'k');
h = polar(Y(:,2)+pi/2, sma*(1-ecc^2)./(1+ecc*cos(Y(:,2))) );
set(h, 'linewidth', 1, 'linestyle', '--');
axis equal
set(gca, 'plotBoxAspectRatio', [3 1 1]);
legend('Trajectory', 'Moon surface', 'Transfer orbit', 'location', 'southeast');
grid on


%On screen display
disp('Final mass [kg]:');
disp(m0 - T/Isp/g0*TIME(end));
disp(' ');
disp('Optimal point found:');
disp([lambdarho0, x]);

warning(orig_state);


% ----------------------------- AUXILIARY FUNCTIONS ----------------------------

    function f = objective(x)
    % Optimization Objective Function
    warning('off','MATLAB:ode45:IntegrationTolNotMet'); %Turn off some ode45 warning
    y0 = [rho0 theta0 v0 u0 lambdarho0 x(1) x(2)]';   %Set initial conditions vector
    [~,y] = ode45(@landingSystem,[0 maxtime],y0,odeopt); %LANDING NUMERICAL INTEGRATION
    f = (vf - y(end,3) )^2 + (uf - y(end,4) )^2;    %Obj function value computation
    end

    function dy = landingSystem(t,y)
    % Planetary Landing Dynamic System
    % y(1) = rho
    % y(2) = theta
    % y(3) = v
    % y(4) = u
    % y(5) = lambda_rho
    % y(6) = lambda_v
    % y(7) = lambda_u
    %-------------------------------------
    beta = atan(y(6)/y(7)) + pi;    %Thrust angle (control angle)
    m = m0 - T/Isp/g0*t;            %Mass (known, linear dynamics, EXACT)
    %-------------------------------------
    %Derivatives
    dy = zeros(7,1);
    dy(1) =  y(3);
    dy(2) =  y(4)/y(1);
    dy(3) =  y(4)^2/y(1) - mu_moon/y(1)^2 + T/m*sin(beta);
    dy(4) = -y(3)*y(4)/y(1) + T/m*cos(beta);
    dy(5) =  y(6)*( (y(4)/y(1))^2 - 2*mu_moon/y(1)^3 ) - y(7)*y(3)*y(4)/y(1)^2;
    dy(6) = -y(5) + y(7)*y(4)/y(1);
    dy(7) = -y(6)*2*y(4)/y(1) + y(7)*y(3)/y(1);
    end

    function [value,isterminal,direction] = events(t,y) %#ok<INUSL>
    % Numerical integration STOPPING FUNCTION
    % Integration is stopped as the lander reaches the target altitude.
        value = y(1) - r_moon - hf;
        isterminal = 1;
        direction = 0;
    end


end

