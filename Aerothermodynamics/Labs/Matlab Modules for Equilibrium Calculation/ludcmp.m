% LU decomposition
function [fjac,indx]=ludcmp(jac,N_var)
 
for i = 1:N_var;
    indx(i) = 1;
end

    d = 1.;
    for i = 1:N_var;

      aa_mx = 0.;
      for j = 1:N_var;
        if( abs(jac(i,j)) > aa_mx )
            aa_mx = abs(jac(i,j));
        end
      end

%      if( aa_mx == zero ) 
%      break 'Singular Matrix'
%      end

      vv(i) = 1. / aa_mx;

    end

% --- LU decomposition

    for j = 1:N_var;

      if( j > 1 )
        for i = 1:j-1

          sum = jac(i,j);

          if( i > 1 )
            for k = 1:i-1
              sum = sum - jac(i,k) * jac(k,j);
            end

            jac(i,j) = sum;

          end

        end

      end

      aa_mx = 0.;
      for i = j:N_var;

        sum = jac(i,j);

        if( j > 1 )
          for k = 1:j-1;
            sum = sum - jac(i,k) * jac(k,j);
          end

          jac(i,j) = sum;

        end

        dum = vv(i) * abs(sum);

        if( dum >= aa_mx )
          i_mx  = i;
          aa_mx = dum;
        end

      end

      if(j ~= i_mx)

        for k = 1:N_var;
            dum          = jac(i_mx,k);
            jac(i_mx,k) = jac(j,k);
            jac(j,k)    = dum;
        end

        d = -d;
        vv(i_mx) = vv(j);

      end

      indx(j) = i_mx;

      if( j ~= N_var)

        if( jac(j,j) == 0. ) 
            jac(j,j) = 1.E-20;
        end

        dum = 1. / jac(j,j);
        for i = j+1:N_var;
          jac(i,j) = jac(i,j) * dum;
        end

      end

    end

    if( jac(N_var,N_var) == 0. )
        jac(N_var,N_var) = 1.E-20;
    end

    fjac = jac;

return
