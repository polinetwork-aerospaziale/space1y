function [E,P,P_rho,P_T,e_sp,h_sp,s_speed,rho_sp]=ideal_gas_Trho(T,rho)

air_phys_properties;

    rho_sp = [eta_O2_std*mol_mass(1)*rho eta_N2_std*mol_mass(2)*rho 0 0 0];

    R = (eta_O2_std + eta_N2_std) * Ru;

    P       = rho * R * T
    E       = P / (gam - 1.);
    P_rho   = R * T;
    P_T     = rho * R;
    e_sp    = E / rho;
    h_sp    = e_sp + P/rho;
    s_speed = sqrt( gam * R * T);

return