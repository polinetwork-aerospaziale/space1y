% Equilibrium calculation at given T,rho

function [E,P,P_rho,P_T,e_sp,h_sp,s_speed,entropy,rho_sp]=equil_Trho(T,rho)

air_phys_properties;
N_sp  = 5;
N_var = 5;

% Initialize unknown molar concentrations and temperature (assume Cv=713)

C_in_O2 = rho * eta_O2_std;
C_in_N2 = rho * eta_N2_std;

if( T<500 )
 C = [C_in_O2*(1-0.02) C_in_N2*(1-0.02) C_in_O2*0.01 C_in_O2*0.01 C_in_N2*0.02];
elseif( T>1500 )
 C = [C_in_O2*(1-0.2) C_in_N2*(1-0.2) C_in_O2*0.1 C_in_O2*0.1 C_in_N2*0.2];
else
 C = [C_in_O2*(1-0.04) C_in_N2*(1-0.04) C_in_O2*0.02 C_in_O2*0.02 C_in_N2*0.04];
end

% Initialize vector of unknowns

x = [sqrt(C(1)) sqrt(C(2)) C(3) C(4) C(5)]';


% Absolute Newton-Raphson solution

errf_mx = 1e-06;
errx_mx = 1e-05;
Nit_mx  = 5000;

errf = 1.;
errx = 1.;
it = 1;

%............ Start loop

while (it < Nit_mx) && ( (errf > errf_mx ) || (errx > errx_mx) );

    [fn,fjac,~]=jacobian_Trho(x,T,rho);


    errf = 0.;
    for i = 1:N_var;
      errf = max( errf, abs( fn(i) ) );
    end

% ..... LU decomposition
    jac = fjac;
    [fjac,indx]= ludcmp(jac,N_var);

% ..... LU backsubstitution
          
    [rhs]=lubksn(fn,fjac,indx,N_var);

% ..... update of the unknowns
%    rhs = fn / fjac;

    errx = 0.;
    for i = 1:N_var;
      errx = max( errx, abs( rhs(i) ) );
      x(i) = abs( x(i) + rhs(i) );
    end

    it = it + 1;

end
%........ end loop


% ---------- Gas properties evaluation
      zero = 0.;
      two  = 2.;
% ..... jacobian matrix evaluation
      [~,fjac,dKeq_dT]=jacobian_Trho(x,T,rho);

% ..... system partial derivatives versus T at costant rho (ds_T)
      ds_T = [x(1)*dKeq_dT(1) x(2)*dKeq_dT(2) x(1)*x(2)*dKeq_dT(3) 0 0];
% ..... system partial derivatives versus rho at costant T (ds_rho)
      ds_rho = [0 0 0 two*eta_O2_std two*eta_N2_std];

% ... LU decomposition
      jac = fjac;
      [fjac,indx]= ludcmp(jac,N_var);

% ... LU backsubstitution
% ... partial derivatives of concentrations versus T at costant rho
      [rhs]=lubksn(ds_T,fjac,indx,N_var);
%      rhs = ds_T / fjac;
     
      ds_T    = rhs;
      ds_T(1) = two * x(1) * ds_T(1);
      ds_T(2) = two * x(2) * ds_T(2);

% ... partial derivatives of concentrations versus rho at costant T
      [rhs]=lubksn(ds_rho,fjac,indx,N_var);
%      rhs = ds_rho / fjac;

      ds_rho    = rhs;
      ds_rho(1) = two * x(1) * ds_rho(1);
      ds_rho(2) = two * x(2) * ds_rho(2);

% ... extract species partial densities and temperature
      rho_sp = [0 0 0 0 0];
      x(1) = x(1)^2;
      x(2) = x(2)^2;
      for i = 1:N_sp;
         rho_sp(i) = mol_mass(i) * x(i);
      end
% writing
      mass_frac = rho_sp / rho
% ... compute partial derivatives of pressure (P_E, P_rho), mixture specific constant (R),
%                   pressure (P), mixture enthalpy (ent), mixture speed of sound (c),
%                   mixture specific entropy
      entropy     = zero;
      mms_sp      = [0 0 0 0 0];
      par1        = log( T/T_ref );
      sum_RoverM  = zero;
      sum_dsp_T   = zero;
      sum_dsp_rho = zero;
      for i = 1:N_sp;
         sum_RoverM  = sum_RoverM + x(i);
         sum_dsp_T   = sum_dsp_T + ds_T(i);
         sum_dsp_rho = sum_dsp_rho + ds_rho(i);

         mms_sp(i) = -log( rho_sp(i)/rho_ref );
       
         if( (i <= 2) || (i == 5) )
            bi   = 2.5;
            exp1 = exp( T_vibr(i)/T );
            par1 = exp1 - 1.;
            mme_sp(i)  = bi*Ru*T + Ru*T_vibr(i)/par1 + heat_form_0K(i);
            mmCv_sp(i) = bi*Ru + ((T_vibr(i)/(T*par1))^2)*exp1*Ru;
            exp0 = exp( -T_vibr(i)/T_ref ); 
            exp1 = exp( T_vibr(i)/T );
        mms_sp(i) =  mms_sp(i) + log((1.-exp0)/(1.-1./exp1)) + (T_vibr(i)/T)/(exp1-1.);
         else
            bi   = 1.5;
            mme_sp(i)  =  bi*Ru*T + heat_form_0K(i);
            mmCv_sp(i) = bi*Ru;
         end
         mms_sp(i) = ( mms_sp(i) + bi * par1 ) * Ru;
         entropy   = entropy  + x(i) * mms_sp(i);
      end
      entropy = entropy / rho;

%... compute mixture internal energy per unit volume  E, specific heat Cv and specific entropy
      E       = zero;
      Cv      = zero;
      for i = 1:N_sp;
         E       = E  + x(i) * mme_sp(i);
         Cv      = Cv + x(i) * mmCv_sp(i) + mme_sp(i) * ds_T(i);
      end
      Cv      = Cv / rho
      Eout    = E

% ... compute partial derivatives of pressure (P_T, P_rho), mixture specific constant (R),
%                   pressure (P), mixture enthalpy (h_sp), mixture speed of sound (c)
      
      
      R       = sum_RoverM * Ru / rho
      P       = rho * R * T
      e_sp    = E / rho;
      h_sp    = e_sp + R * T;
      P_T     = Ru * ( T * sum_dsp_T + sum_RoverM );
      P_rho   = Ru * T * sum_dsp_rho;
      s_speed = sqrt( P_rho + (T * P_T * P_T) / (Cv * rho * rho) )


return