clear all;
% aL = input('aL : ');
% bL = input('bL : ');
% cL = input('cL : ');
% dL = input('dL : ');
% aR = input('aR : ');
% bR = input('bR : ');
% cR = input('cR : ');
% dR = input('dR : ');
% xT = input('xT : ');
% xE = input('xE : ');
aL = 2.;
bL = -3.;
cL = 0.;
dL = 2.0;
aR = -3/8 ;
bR = 9/4 ;
cR = -27/8;
dR = 2.5 ;
xT = 1.0;
xE = 3.0 ;

P0 = input('P0 : ');
T0 = input('T0 : ');

gamma = 1.4
%AL polynomes � gauche (x entre 0 et xT) et � droite (x entre xT et xE)
AL = [aL bL cL dL];
AR = [aR bR cR dR];
%Ke : Ae/A*
Ke = polyval(AR,xE)/polyval(AR,xT);
%f : fonction A/A* = f(M)
f = @(M) (1/M)*((2/(gamma+1))*(1+((gamma-1)/2)*M^2))^((gamma+1)/(2*(gamma-1)))-Ke ;

% MeA : nombre de mach en sortie subsonique, en isentropique

% MeB : nombre de mach en sortie supersonique, en isentropique
MeA = fzero(f,[0.01 0.999]) ;
MeB = fzero(f,[1.01 10]) ;
%MeC nombre de mach en sortie si choc � la sortie
MeC = sqrt((1+((gamma-1)/2)*MeB^2)/(gamma*MeB^2-(gamma-1)/2));

PeA = P0*(1+(gamma-1)/2*MeA^2)^(-gamma/(gamma-1)) ;
PeB = P0*(1+(gamma-1)/2*MeB^2)^(-gamma/(gamma-1)) ;
PeC = PeB*(1+2*gamma/(gamma+1)*(MeB^2-1)) ;

