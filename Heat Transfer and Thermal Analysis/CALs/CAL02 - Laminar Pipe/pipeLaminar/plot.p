# Gnuplot script file for plotting data

set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
set xtic auto                          # set xtics automatically
set ytic auto                          # set ytics automatically
set key left top

fileU = './postProcessing/graph/150/verticalLine_U.xy'
fileT = './postProcessing/graph/150/verticalLine_T.xy'
nu = `cat constant/transportProperties | grep -w 'nu' | cut -d ' ' -f22`
DT = `cat constant/transportProperties | grep -w 'DT' | cut -d ' ' -f22`
r = `cat system/blockMeshDict | grep  -w 'radius' | cut -d ' ' -f3` 
dTdn = `cat 0/T | grep -w 'gradient' | cut -d ' ' -f18` 
ub = `cat 0/U | grep -w 'internalField' | cut -d ' ' -f8` 

Pr = nu/DT
Re = ub/nu
dT_L = 4*dTdn/Re/Pr

plot fileT using 1:2
Tw = GPVAL_DATA_Y_MAX
# Uncomment following lines to print values in shell
#print 'Tw = ', Tw
#print 'nu = ', nu
#print 'r = ', r
#print 'DT = ', DT
#print 'Pr = ', Pr
#print 'dTdn = ', dTdn
#print 'ub = ', ub
#print 'dT_L = ', dT_L

set term png enhanced font "Helvetica,15" 
set output "T.png"
set xlabel 'r'
set ylabel 'T'
T(x) = Tw - 2*ub*r**2*Pr/nu*dT_L*(3.0/16 + 1.0/16*(x/r)**4 - 1.0/4*(x/r)**2) 
plot [0:r] T(x) ls 1 lw 2 t 'exact' ,\
fileT using 1:2 w p pt 6 ps 2 t 'OF'

set output "U.png"
set key right top
set xlabel 'r'
set ylabel 'U'
u(x) = ub*2*(1 - (x/r)**2)
plot [0:r] u(x)/ub ls 1 lw 2 t 'exact' ,\
fileU using 1:4 w p pt 6 ps 2 t 'OF'
