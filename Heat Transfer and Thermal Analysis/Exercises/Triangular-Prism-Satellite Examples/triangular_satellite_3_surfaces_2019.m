clear

sigma=5.67e-8;
Ep2_sun=1350;
eps1=0.8;
eps2=0.25;
eps3=0.25;
alpha1=eps1;
A1=1;
A2=1;
A3=1;

%case 2 or 4 of Table 10.2 p. 545 L&L 4th ed.
F12=1-sin((pi/3)/2);
F13=F12;
F21=F12;
F23=F12;
F31=F12;
F32=F12;
%F31=(A3+A1-A2)/(2*A3);

R1=(1-eps1)/(eps1*A1);
R2=(1-eps2)/(eps2*A2);
R3=(1-eps3)/(eps3*A3);

R12=1/(A1*F12);
R13=1/(A1*F13);
R21=1/(A2*F21);
R23=1/(A2*F23);
R31=1/(A3*F31);
R32=1/(A3*F32);

%creation of the coefficient matrix
%6 unknowns: J1,J2,J3,Eb1,Eb2,Eb3
CM=zeros(6,6);

%1st equation
CM(1,1)=1/R1;
CM(1,2)=0;
CM(1,3)=0;
CM(1,4)=-(1/R1+eps1*A1);
CM(1,5)=0;
CM(1,6)=0;

%2nd equation
CM(2,1)=0;
CM(2,2)=1/R2;
CM(2,3)=0;
CM(2,4)=0;
CM(2,5)=-(1/R2+eps2*A2);
CM(2,6)=0;

%3rd equation
CM(3,1)=0;
CM(4,2)=0;
CM(3,3)=1/R3;
CM(3,4)=0;
CM(3,5)=0;
CM(3,6)=-(1/R3+eps3*A3);

%4th equation
CM(4,1)=(1/R1+1/R12+1/R13);
CM(4,2)=-1/R12;
CM(4,3)=-1/R13;
CM(4,4)=-1/R1;
CM(4,5)=0;
CM(4,6)=0;

%5th equation
CM(5,1)=-1/R21;
CM(5,2)=(1/R2+1/R21+1/R23);
CM(5,3)=-1/R23;
CM(5,4)=0;
CM(5,5)=-1/R2;
CM(5,6)=0;

%6th equation
CM(6,1)=-1/R31;
CM(6,2)=-1/R32;
CM(6,3)=(1/R3+1/R31+1/R32);
CM(6,4)=0;
CM(6,5)=0; 
CM(6,6)=-1/R3;

%creation of the vector of the known terms
KT=zeros(6,1);
KT(1)=-alpha1*Ep2_sun*A1; %the only known term different from 0 is in the top equation, due to Sun radiation

%solution of the system
Val=CM\KT;

for i=1:3
    J(i)=Val(i);
end
for i=1:3
    T(i)=(Val(i+3)/sigma)^(1/4);
end

Qp1=(sigma*T(1)^4-J(1))/R1;
Qp2=(sigma*T(2)^4-J(2))/R2;
Qp3=(sigma*T(3)^4-J(3))/R3;

Qp1_ver=(alpha1*Ep2_sun-eps1*sigma*T(1)^4)*A1;
Qp2_ver=-eps2*sigma*T(2)^4*A2;
Qp3_ver=-eps3*sigma*T(3)^4*A3;

Qp12=(J(1)-J(2))*A1*F12;
Qp13=(J(1)-J(3))*A1*F13;


%analytical solution v.1
F12a=1;
A2a=2*A2;
Ra=(1-eps1)/(eps1*A1)+1/(A1*F12a)+(1-eps2)/(eps2*A2a);
Eb2a_v1=(alpha1*Ep2_sun*A1)/(eps2*A2a+eps1*A1*(eps2*A2a*Ra+1));
Eb1a_v1=(eps2*A2a*Ra+1)*Eb2a_v1;
Qpa_v1=eps2*A2a*Eb2a_v1;
T1a_v1=(Eb1a_v1/sigma)^(1/4);
T2a_v1=(Eb2a_v1/sigma)^(1/4);

%analytical solution v.2
Qpa_v2=(alpha1*Ep2_sun*A1)/(1+eps1*A1*Ra+(eps1*A1)/(eps2*A2a));
T1a_v2=(Qpa_v2/sigma*(Ra+1/(eps2*A2a)))^(1/4);
T2a_v2=(Qpa_v2/(eps2*sigma*A2a))^(1/4);

%checks
Qp_ver1=(alpha1*Ep2_sun-eps1*sigma*T1a_v1^4)*A1;
Qp_ver2=-sigma*(T2a_v1^4-T1a_v1^4)/Ra;
Qp_ver3=eps2*sigma*A2*T2a_v1^4;

ver_eqz_bil_1=(eps1*Ep2_sun-eps1*sigma*T1a_v1^4)*A1-sigma*(T1a_v1^4-T2a_v1^4)/Ra;
ver_eqz_bil_2=-sigma*(T2a_v1^4-T1a_v1^4)/Ra-eps2*sigma*A2a*T2a_v1^4;


%SALVATAGGIO RISULTATI
vars=who;
save('dummy_workspace_saving.mat');
M_save_results_Latex_table4C(vars,'results_esX.txt','%.2f','dummy_workspace_saving.mat');