function [found,int_xyz] = int_ray_tri(or,di,tv1,tv2,tv3)
%Ray-triangle intersection using the algorithm proposed by Moller and Trumbore (1997):
%   http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
%implemented as described in:
%   http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection
%
%Input (all 3-component vectors):
%   or: origin of the ray
%   di: direction
%   tv1,tv2,tv3: triangle vertices
%Output:
%   found: 1 intersection found, 0 no intersection found
%   int_xyz: coordinates of the intersection point

small=1e-6;

e1=tv2-tv1;
e2=tv3-tv1;
h=cross(di,e2);
a=dot(e1,h);

if abs(a)<small % vector and plane are parallel
   found=0;
   int_xyz=[NaN NaN NaN];
   return;
end;

f=1/a;
s=or-tv1;
u=f*dot(s,h);

if u<0.0 || u >1.0
   %intersection between ray and triangle plane is outside of the triangle
   found=0;
   int_xyz=[NaN NaN NaN];
   return;          
end;

q=cross(s,e1);
v=f*dot(di,q);

if v<0.0 || u+v>1.0
   %intersection is again outside of the triangle
   found=0;
   int_xyz=[NaN NaN NaN];
   return;
end;

%t=distance of the intersection from the ray origin
t=f*dot(e2,q);

if t<small %in this case there is a line intersection but not a ray intersection
   found=0;
   int_xyz=[NaN NaN NaN];
   return;
end;

found=1;
int_xyz=or+t*di;

end