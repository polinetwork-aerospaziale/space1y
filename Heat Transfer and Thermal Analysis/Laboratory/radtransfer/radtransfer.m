 
function varargout = radtransfer(varargin)
% radtransfer Application M-file for radtransfer.fig
%    FIG = radtransfer launch radtransfer GUI.
%    radtransfer('callback_name', ...) invoke the named callback.

% Last Modified by GUIDE v2.0 07-Jul-2004 09:42:25

if nargin == 0  % LAUNCH GUI

	fig = openfig(mfilename,'reuse');

	% Use system color scheme for figure:
	set(fig,'Color',get(0,'defaultUicontrolBackgroundColor'));

	% Generate a structure of handles to pass to callbacks, and store it. 
	handles = guihandles(fig);
    handles.nomfenetre='View factors and radiation heat transfers';
    handles.figureindex=1;
    handles.nombrefigures=1;
    handles.figure(1).connue=1;
    handles.figure(1).nom='Figure 1';
    handles.figure(1).emissivite=1;
    handles.figure(1).active=1;
    handles.figure(1).nombrepoints=3;
    handles.figure(1).type=1;
    handles.figure(1).centre=[0 0 0];
    handles.figure(1).normale=[0 0 1];
    handles.figure(1).rayon=1;

    set(fig,'name',handles.nomfenetre);
    for i=1:3
        for j=1:3
            handles.figure(1).coord(i,j)=0;
        end
    end
    
	guidata(fig, handles);

	if nargout > 0
		varargout{1} = fig;
	end
    
    %initialisation

elseif ischar(varargin{1}) % INVOKE NAMED SUBFUNCTION OR CALLBACK

	try
		if (nargout)
			[varargout{1:nargout}] = feval(varargin{:}); % FEVAL switchyard
		else
			feval(varargin{:}); % FEVAL switchyard
		end
	catch
		disp(lasterr);
	end

end

% --------------------------------------------------------------------
function varargout = lstfigures_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);

handles.figureindex=get(handles.lstfigures,'value');
guidata(h,handles);

afficher(h, eventdata, handles, varargin);



% --------------------------------------------------------------------
function varargout = txtbilanrad_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = txtemissivite_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = chkactive_Callback(h, eventdata, handles, varargin)

if get(handles.chkactive,'value')==1
    if handles.figure(handles.figureindex).connue==1
        if str2num(get(handles.txttemperature,'string'))==[]
            msgbox('To activate the figure, you have to enter the temperature!','error');
            set(handles.chkactive,'value',0);    
        end
    end
    if handles.figure(handles.figureindex).connue==2
        if str2num(get(handles.txtbilanrad,'string'))==[]
            msgbox('To activate the figure, you have to enter the radiative balance!','error');
            set(handles.chkactive,'value',0);    
        end
    end
    if str2num(get(handles.txtemissivite,'string'))==[]
            msgbox('To activate the figure, you have to enter the emissivity!','error');
            set(handles.chkactive,'value',0);   
    end
end
if get(handles.chkactive,'value')==1
    handles.figure(handles.figureindex).active=1;
else
    handles.figure(handles.figureindex).active=0;
end

actualiser(h, eventdata, handles, varargin)

% --------------------------------------------------------------------
function varargout = rdbtemperature_Callback(h, eventdata, handles, varargin)

radioselect(h, eventdata, handles, varargin,1);


% --------------------------------------------------------------------
function varargout = rdbbilanrad_Callback(h, eventdata, handles, varargin)


radioselect(h, eventdata, handles, varargin,2);



% --------------------------------------------------------------------
function varargout = cmdactualiser_Callback(h, eventdata, handles, varargin)


graphique(h,eventdata,handles,varargin);

% --------------------------------------------------------------------
function varargout = cmdeffacer_Callback(h, eventdata, handles, varargin)

effacer(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = cmdajouter_Callback(h, eventdata, handles, varargin)

%Add a new figure
actualiser(h, eventdata, handles, varargin);

handles.nombrefigures=handles.nombrefigures+1;
handles.figureindex=handles.nombrefigures;
handles.figure(handles.figureindex).nom=['Figure ',num2str(handles.figureindex)];
handles.figure(handles.figureindex).connue=1;
handles.figure(handles.figureindex).temperature='';
handles.figure(handles.figureindex).bilanrad='';
handles.figure(handles.figureindex).emissivite=1;
handles.figure(handles.figureindex).nombrepoints=3;
handles.figure(handles.figureindex).active=1;
handles.figure(handles.figureindex).type=1;
handles.figure(handles.figureindex).centre=[0 0 0];
handles.figure(handles.figureindex).normale=[0 0 1];
handles.figure(handles.figureindex).rayon=1;

for i=1:10
    for j=1:3
        handles.figure(handles.figureindex).coord(i,j)=0;
    end
end

guidata(gcbo,handles);

afficher(h, eventdata, handles, varargin);
actualiser(h, eventdata, handles, varargin);
set(handles.lstfigures,'value',handles.figureindex);

% --------------------------------------------------------------------
function varargout = cmdenregistrer_Callback(h, eventdata, handles, varargin)

enregistrer(h, eventdata, handles, varargin)


% --------------------------------------------------------------------
function varargout = cmdgraphique_Callback(h, eventdata, handles, varargin)

%Determine in which file the picture will be saved.
[nomfichier,chemin] = uiputfile({'*.jpg','Jpg files (*.jpg)'},'Save as...');
if nomfichier==0
    return
end

if nomfichier((length(nomfichier)-3):(length(nomfichier)))~='.jpg'
    nomfichier=[nomfichier,'.jpg'];
end

print (gcf,'-djpeg99','-r0',nomfichier);
img=imread(nomfichier,'jpg');
[hauteur,largeur]=size(img);
imagesortie=img((hauteur-300):hauteur,1:400,:);
imwrite(imagesortie,nomfichier,'jpg');



% --------------------------------------------------------------------
function varargout = cmdchargement_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = cmdnouveau_Callback(h, eventdata, handles, varargin)


%Create a new list of figure

clear handles.figure;

handles.nombrefigures=1;
handles.figureindex=1;
handles.figure(handles.figureindex).nom=['Figure ',num2str(handles.figureindex)];
handles.figure(handles.figureindex).connue=1;
handles.figure(handles.figureindex).temperature='';
handles.figure(handles.figureindex).bilanrad='';
handles.figure(handles.figureindex).emissivite=1;
handles.figure(handles.figureindex).nombrepoints=3;
handles.figure(handles.figureindex).active=0;
handles.figure(1).type=1;
handles.figure(1).centre=[0 0 0];
handles.figure(1).normale=[0 0 1];
handles.figure(1).rayon=1;

for i=1:10
    for j=1:3
        handles.figure(handles.figureindex).coord(i,j)=0;
    end
end

guidata(gcbo,handles);

afficher(h, eventdata, handles, varargin);
actualiser(h, eventdata, handles, varargin);
set(handles.lstfigures,'value',handles.figureindex);

cla;
% --------------------------------------------------------------------
function varargout = cmdcalcul_Callback(h, eventdata, handles, varargin)



calcul(h, eventdata, handles, varargin);
        
        
        
        
% --------------------------------------------------------------------
function varargout = cmdaide_Callback(h, eventdata, handles, varargin)

!notepad radtransfer (GUI) - readme.txt


% --------------------------------------------------------------------
function varargout = cmdQuitter_Callback(h, eventdata, handles, varargin)

close;


% --------------------------------------------------------------------
function varargout = figure1_CreateFcn(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = cmdouvrir_Callback(h, eventdata, handles, varargin)

ouvrir(h, eventdata, handles, varargin)

%-------------------------------------------------------------------------
function varargout = radioselect(h, eventdata, handles, varargin,click)


if click==1 %Temperature is known
    handles.figure(handles.figureindex).connue=1;
    set(handles.rdbbilanrad,'value',0);
    set(handles.rdbtemperature,'value',1);
    set(handles.txttemperature,'enable','On');
    set(handles.txtbilanrad,'enable','Off');
elseif click==2 %Radiative balance known.
    handles.figure(handles.figureindex).connue=2;
    set(handles.rdbbilanrad,'value',1);
    set(handles.rdbtemperature,'value',0);
    set(handles.txttemperature,'enable','Off');
    set(handles.txtbilanrad,'enable','On');
end


guidata(gcbo,handles);


% --------------------------------------------------------------------
function varargout = txtnomfigure_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);

%---------------------------------------------------------------------
function varargout = actualiser(h, eventdata, handles, varargin)

%Actualisation of the input for the current figure.
if get(handles.rdbtemperature,'value')==1
    handles.figure(handles.figureindex).connue=1;
elseif get(handles.rdbbilanrad,'value')==1
    handles.figure(handles.figureindex).connue=2;
end

handles.figure(handles.figureindex).temperature=str2num(get(handles.txttemperature,'string'));
handles.figure(handles.figureindex).nom=get(handles.txtnomfigure,'string');
handles.figure(handles.figureindex).bilanrad=str2num(get(handles.txtbilanrad,'string'));
handles.figure(handles.figureindex).emissivite=str2num(get(handles.txtemissivite,'string'));

handles.figure(handles.figureindex).type=get(handles.ppmtype,'value');
handles.figure(handles.figureindex).centre=[str2num(get(handles.txtXcentre,'string')),str2num(get(handles.txtYcentre,'string')),str2num(get(handles.txtZcentre,'string'))];
handles.figure(handles.figureindex).normale=[str2num(get(handles.txtXnormale,'string')),str2num(get(handles.txtYnormale,'string')),str2num(get(handles.txtZnormale,'string'))];
handles.figure(handles.figureindex).rayon=str2num(get(handles.txtrayon,'string'));



guidata(gcbo,handles);


%The variable spacer is to have the same number of caracters in each lines
%because the listbox object only accept matrix as string parameter.
%[A] is for activated and [D] is for desactivated.

for i=1:handles.nombrefigures
    longtexte(i)=length(num2str(i))+1+length(handles.figure(i).nom);
end
longtextemax=max(longtexte);
for i=1:handles.nombrefigures
    spacer='';
    if (longtextemax-longtexte(i))>0
        for j=1:(longtextemax-longtexte(i))
            spacer=[spacer,' '];
        end
    end
    if handles.figure(i).active==1
        spacer=[spacer,' [A]'];
    else
        spacer=[spacer,' [D]'];
    end
   texte(i,:)=[num2str(i),':',handles.figure(i).nom,spacer];

end

set(handles.lstfigures,'string',texte);


handles.figure(handles.figureindex).active=get(handles.chkactive,'value');
    

erreur=0;
nbpoints=0;
if isempty(get(handles.txtX1,'string'))==0 & isempty(get(handles.txtY1,'string'))==0 & isempty(get(handles.txtZ1,'string'))==0
    handles.figure(handles.figureindex).coord(1,1)=str2num(get(handles.txtX1,'string'));
    handles.figure(handles.figureindex).coord(1,2)=str2num(get(handles.txtY1,'string'));
    handles.figure(handles.figureindex).coord(1,3)=str2num(get(handles.txtZ1,'string'));
elseif erreur==0
    erreur=1;
 
end

if isempty(get(handles.txtX2,'string'))==0 & isempty(get(handles.txtY2,'string'))==0 & isempty(get(handles.txtZ2,'string'))==0
    handles.figure(handles.figureindex).coord(2,1)=str2num(get(handles.txtX2,'string'));
    handles.figure(handles.figureindex).coord(2,2)=str2num(get(handles.txtY2,'string'));
    handles.figure(handles.figureindex).coord(2,3)=str2num(get(handles.txtZ2,'string'));
elseif erreur==0
    erreur=1;
end

if isempty(get(handles.txtX3,'string'))==0 & isempty(get(handles.txtY3,'string'))==0 & isempty(get(handles.txtZ3,'string'))==0
    handles.figure(handles.figureindex).coord(3,1)=str2num(get(handles.txtX3,'string'));
    handles.figure(handles.figureindex).coord(3,2)=str2num(get(handles.txtY3,'string'));
    handles.figure(handles.figureindex).coord(3,3)=str2num(get(handles.txtZ3,'string'));
elseif erreur==0
    erreur=1;
end

%At the 4th point, we check if there is coordinates entered (if there is a
%point)
if isempty(get(handles.txtX4,'string'))==0 & isempty(get(handles.txtY4,'string'))==0 & isempty(get(handles.txtZ4,'string'))==0
handles.figure(handles.figureindex).coord(4,1)=str2num(get(handles.txtX4,'string'));
handles.figure(handles.figureindex).coord(4,2)=str2num(get(handles.txtY4,'string'));
handles.figure(handles.figureindex).coord(4,3)=str2num(get(handles.txtZ4,'string'));
elseif nbpoints==0
    nbpoints=3;
end
if isempty(get(handles.txtX5,'string'))==0 & isempty(get(handles.txtY5,'string'))==0 & isempty(get(handles.txtZ5,'string'))==0
handles.figure(handles.figureindex).coord(5,1)=str2num(get(handles.txtX5,'string'));
handles.figure(handles.figureindex).coord(5,2)=str2num(get(handles.txtY5,'string'));
handles.figure(handles.figureindex).coord(5,3)=str2num(get(handles.txtZ5,'string'));
elseif nbpoints==0
    nbpoints=4;
end
if isempty(get(handles.txtX6,'string'))==0 & isempty(get(handles.txtY6,'string'))==0 & isempty(get(handles.txtZ6,'string'))==0
handles.figure(handles.figureindex).coord(6,1)=str2num(get(handles.txtX6,'string'));
handles.figure(handles.figureindex).coord(6,2)=str2num(get(handles.txtY6,'string'));
handles.figure(handles.figureindex).coord(6,3)=str2num(get(handles.txtZ6,'string'));
elseif nbpoints==0
    nbpoints=5;
end
if isempty(get(handles.txtX7,'string'))==0 & isempty(get(handles.txtY7,'string'))==0 & isempty(get(handles.txtZ7,'string'))==0
handles.figure(handles.figureindex).coord(7,1)=str2num(get(handles.txtX7,'string'));
handles.figure(handles.figureindex).coord(7,2)=str2num(get(handles.txtY7,'string'));
handles.figure(handles.figureindex).coord(7,3)=str2num(get(handles.txtZ7,'string'));
elseif nbpoints==0
    nbpoints=6;
end
if isempty(get(handles.txtX8,'string'))==0 & isempty(get(handles.txtY8,'string'))==0 & isempty(get(handles.txtZ8,'string'))==0
handles.figure(handles.figureindex).coord(8,1)=str2num(get(handles.txtX8,'string'));
handles.figure(handles.figureindex).coord(8,2)=str2num(get(handles.txtY8,'string'));
handles.figure(handles.figureindex).coord(8,3)=str2num(get(handles.txtZ8,'string'));
elseif nbpoints==0
    nbpoints=7;
end
if isempty(get(handles.txtX9,'string'))==0 & isempty(get(handles.txtY9,'string'))==0 & isempty(get(handles.txtZ9,'string'))==0
handles.figure(handles.figureindex).coord(9,1)=str2num(get(handles.txtX9,'string'));
handles.figure(handles.figureindex).coord(9,2)=str2num(get(handles.txtY9,'string'));
handles.figure(handles.figureindex).coord(9,3)=str2num(get(handles.txtZ9,'string'));
nbpoints=9;
elseif nbpoints==0
    nbpoints=8;
end


handles.figure(handles.figureindex).nombrepoints=nbpoints;
for i=(nbpoints+1):10
    handles.figure(handles.figureindex).coord(i,1)=handles.figure(handles.figureindex).coord(1,1);
    handles.figure(handles.figureindex).coord(i,2)=handles.figure(handles.figureindex).coord(1,2);
    handles.figure(handles.figureindex).coord(i,3)=handles.figure(handles.figureindex).coord(1,3);
end

    
guidata(gcbo,handles);

% --------------------------------------------------------------------
function varargout = afficher(h, eventdata, handles, varargin)

%Fill the boxes with the properties of the current figure.
set(handles.txtnomfigure,'string',handles.figure(handles.figureindex).nom);
set(handles.txttemperature,'string',num2str(handles.figure(handles.figureindex).temperature));
set(handles.txtbilanrad,'string',num2str(handles.figure(handles.figureindex).bilanrad));
set(handles.txtemissivite,'string',num2str(handles.figure(handles.figureindex).emissivite));
set(handles.chkactive,'value',handles.figure(handles.figureindex).active);

set(handles.ppmtype,'value',handles.figure(handles.figureindex).type);
set(handles.txtXcentre,'string',num2str(handles.figure(handles.figureindex).centre(1)));
set(handles.txtYcentre,'string',num2str(handles.figure(handles.figureindex).centre(2)));
set(handles.txtZcentre,'string',num2str(handles.figure(handles.figureindex).centre(3)));
set(handles.txtXnormale,'string',num2str(handles.figure(handles.figureindex).normale(1)));
set(handles.txtYnormale,'string',num2str(handles.figure(handles.figureindex).normale(2)));
set(handles.txtZnormale,'string',num2str(handles.figure(handles.figureindex).normale(3)));
set(handles.txtrayon,'string',num2str(handles.figure(handles.figureindex).rayon));

visibilite(h, eventdata, handles, varargin);

%Depending of which variable is know, the correct radio button is selected.
if handles.figure(handles.figureindex).connue==1
    set(handles.rdbbilanrad,'value',0);
    set(handles.rdbtemperature,'value',1);
    set(handles.txttemperature,'enable','On');
    set(handles.txtbilanrad,'enable','Off');
elseif handles.figure(handles.figureindex).connue==2
    set(handles.rdbbilanrad,'value',1);
    set(handles.rdbtemperature,'value',0);
    set(handles.txttemperature,'enable','Off');
    set(handles.txtbilanrad,'enable','On');
end



for i=1:handles.nombrefigures
    longtexte(i)=length(num2str(i))+1+length(handles.figure(i).nom);
end
longtextemax=max(longtexte);
for i=1:handles.nombrefigures
    spacer='';
    if (longtextemax-longtexte(i))>0
        for j=1:(longtextemax-longtexte(i))
            spacer=[spacer,' '];
        end
    end
    if handles.figure(i).active==1
        spacer=[spacer,' [A]'];
    else
        spacer=[spacer,' [D]'];
    end
   texte(i,:)=[num2str(i),':',handles.figure(i).nom,spacer];

end

set(handles.lstfigures,'string',texte);

if handles.figure(handles.figureindex).nombrepoints>=1
    set(handles.txtX1,'string',num2str(handles.figure(handles.figureindex).coord(1,1)));
    set(handles.txtY1,'string',num2str(handles.figure(handles.figureindex).coord(1,2)));
    set(handles.txtZ1,'string',num2str(handles.figure(handles.figureindex).coord(1,3)));
else
    set(handles.txtX1,'string','');
    set(handles.txtY1,'string','');
    set(handles.txtZ1,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=2
    set(handles.txtX2,'string',num2str(handles.figure(handles.figureindex).coord(2,1)));
    set(handles.txtY2,'string',num2str(handles.figure(handles.figureindex).coord(2,2)));
    set(handles.txtZ2,'string',num2str(handles.figure(handles.figureindex).coord(2,3)));
else
    set(handles.txtX2,'string','');
    set(handles.txtY2,'string','');
    set(handles.txtZ2,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=3
    set(handles.txtX3,'string',num2str(handles.figure(handles.figureindex).coord(3,1)));
    set(handles.txtY3,'string',num2str(handles.figure(handles.figureindex).coord(3,2)));
    set(handles.txtZ3,'string',num2str(handles.figure(handles.figureindex).coord(3,3)));
else
    set(handles.txtX3,'string','');
    set(handles.txtY3,'string','');
    set(handles.txtZ3,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=4
    set(handles.txtX4,'string',num2str(handles.figure(handles.figureindex).coord(4,1)));
    set(handles.txtY4,'string',num2str(handles.figure(handles.figureindex).coord(4,2)));
    set(handles.txtZ4,'string',num2str(handles.figure(handles.figureindex).coord(4,3)));
else
    set(handles.txtX4,'string','');
    set(handles.txtY4,'string','');
    set(handles.txtZ4,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=5
    set(handles.txtX5,'string',num2str(handles.figure(handles.figureindex).coord(5,1)));
    set(handles.txtY5,'string',num2str(handles.figure(handles.figureindex).coord(5,2)));
    set(handles.txtZ5,'string',num2str(handles.figure(handles.figureindex).coord(5,3)));
else
    set(handles.txtX5,'string','');
    set(handles.txtY5,'string','');
    set(handles.txtZ5,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=6
    set(handles.txtX6,'string',num2str(handles.figure(handles.figureindex).coord(6,1)));
    set(handles.txtY6,'string',num2str(handles.figure(handles.figureindex).coord(6,2)));
    set(handles.txtZ6,'string',num2str(handles.figure(handles.figureindex).coord(6,3)));
else
    set(handles.txtX6,'string','');
    set(handles.txtY6,'string','');
    set(handles.txtZ6,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=7
    set(handles.txtX7,'string',num2str(handles.figure(handles.figureindex).coord(7,1)));
    set(handles.txtY7,'string',num2str(handles.figure(handles.figureindex).coord(7,2)));
    set(handles.txtZ7,'string',num2str(handles.figure(handles.figureindex).coord(7,3)));
else
    set(handles.txtX7,'string','');
    set(handles.txtY7,'string','');
    set(handles.txtZ7,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=8
    set(handles.txtX8,'string',num2str(handles.figure(handles.figureindex).coord(8,1)));
    set(handles.txtY8,'string',num2str(handles.figure(handles.figureindex).coord(8,2)));
    set(handles.txtZ8,'string',num2str(handles.figure(handles.figureindex).coord(8,3)));
else
    set(handles.txtX8,'string','');
    set(handles.txtY8,'string','');
    set(handles.txtZ8,'string','');
end

if handles.figure(handles.figureindex).nombrepoints>=9
    set(handles.txtX9,'string',num2str(handles.figure(handles.figureindex).coord(9,1)));
    set(handles.txtY9,'string',num2str(handles.figure(handles.figureindex).coord(9,2)));
    set(handles.txtZ9,'string',num2str(handles.figure(handles.figureindex).coord(9,3)));
else
    set(handles.txtX9,'string','');
    set(handles.txtY9,'string','');
    set(handles.txtZ9,'string','');
end
graphique(h,eventdata,handles,varargin);
% --------------------------------------------------------------------
function varargout = graphique(h, eventdata, handles, varargin)
n=0;
cla;
for i=1:handles.nombrefigures
    if handles.figure(i).active==1
        n=n+1;
        figactive(n)=handles.figure(i);
        figindex(n)=i;
    end
end

nbpointscercle=100;%This variable determine the number of sides by which the circle are approximated. Here in the
                   %function displaying the graphic, it is normal to have a
                   %greater number of sides because displaying the circle
                   %do not demand a lot of computation.
for i=1:n
    switch figactive(i).type
    case 1 %Polygon
        for j=1:figactive(i).nombrepoints
            X(j,i)=figactive(i).coord(j,1);
            Y(j,i)=figactive(i).coord(j,2);
            Z(j,i)=figactive(i).coord(j,3);
        end
        for j=figactive(i).nombrepoints+1:nbpointscercle
            X(j,i)=figactive(i).coord(1,1);
            Y(j,i)=figactive(i).coord(1,2);
            Z(j,i)=figactive(i).coord(1,3);
        end
    case 2 %Circle
        nx=figactive(i).normale(1);
        ny=figactive(i).normale(2);
        nz=figactive(i).normale(3);
        nx=nx/sqrt(nx^2+ny^2+nz^2);
        ny=ny/sqrt(nx^2+ny^2+nz^2);
        nz=nz/sqrt(nx^2+ny^2+nz^2);


        x0=figactive(i).centre(1);
        y0=figactive(i).centre(2);
        z0=figactive(i).centre(3);
        
        R=figactive(i).rayon;
        if nz==0
            if nx==0&ny==0
                msgbox('The three coordinates of the normal are 0!','ERROR','Error');
                break;
            elseif nx==0
                for j=1:nbpointscercle
                    theta=(j-1)/nbpointscercle*2*pi;
                    
                    xtemp(j)=R*cos(theta);
                    ytemp(j)=0;
                    ztemp(j)=R*sin(theta);

                    X(j,i)=xtemp(j)+x0;
                    Y(j,i)=ytemp(j)+y0;
                    Z(j,i)=ztemp(j)+z0;
                end
            elseif ny==0
                for j=1:nbpointscercle
                    theta=(j-1)/nbpointscercle*2*pi
                    
                    xtemp(j)=0;
                    ytemp(j)=R*cos(theta);
                    ztemp(j)=R*sin(theta);
                    
                    X(j,i)=xtemp(j)+x0;
                    Y(j,i)=ytemp(j)+y0;
                    Z(j,i)=ztemp(j)+z0;

                end
            else
                for j=1:nbpointscercle
                    theta=(j-1)/nbpointscercle*2*pi
                    
                    xtemp(j)=R*cos(theta)*sqrt(1/(1+(nx/ny)^2));
                    ytemp(j)=R*cos(theta)*sqrt(1/(1+(ny/nx)^2));
                    ztemp(j)=R*sin(theta);
                    if nx/ny>0
                        xtemp(j)=-xtemp(j)
                    end
                    
                    X(j,i)=xtemp(j)+x0;
                    Y(j,i)=ytemp(j)+y0;
                    Z(j,i)=ztemp(j)+z0;

                end
            end
        else
            nx=nx+1*10^(-10);
            ny=ny+1*10^(-10);
            nz=nz+1*10^(-10);
            
            theta0=atan(-nx/ny)+0.00001; %The angle theta 0 is to avoid problems with signs of trigonometric functions.
            for j=1:nbpointscercle/2
                theta=theta0+(j-1)/nbpointscercle*2*pi;
                xtemp(j)=-R*cos(theta)*nz/(cos(theta)*nx+sin(theta)*ny)/(nz^2/(cos(theta)*nx+sin(theta)*ny)^2+1)^(1/2);
                ytemp(j)=-R*sin(theta)*nz/(cos(theta)*nx+sin(theta)*ny)/(nz^2/(cos(theta)*nx+sin(theta)*ny)^2+1)^(1/2);
                ztemp(j)=R/(nz^2/(cos(theta)*nx+sin(theta)*ny)^2+1)^(1/2);
            
                xtemp(j+nbpointscercle/2)=-xtemp(j);
                ytemp(j+nbpointscercle/2)=-ytemp(j);
                ztemp(j+nbpointscercle/2)=-ztemp(j);

                X(j,i)=xtemp(j)+x0;
                Y(j,i)=ytemp(j)+y0;
                Z(j,i)=ztemp(j)+z0;
            
                X(j+nbpointscercle/2,i)=xtemp(j+nbpointscercle/2)+x0;
                Y(j+nbpointscercle/2,i)=ytemp(j+nbpointscercle/2)+y0;
                Z(j+nbpointscercle/2,i)=ztemp(j+nbpointscercle/2)+z0;
            end
        end
        
    end   
end

fill3(X,Y,Z,'r');

for i=1:n
    if figactive(i).type==1      
        x=0;
        y=0;
        z=0;
        for j=1:figactive(i).nombrepoints
            x=x+figactive(i).coord(j,1)/figactive(i).nombrepoints;
            y=y+figactive(i).coord(j,2)/figactive(i).nombrepoints;
            z=z+figactive(i).coord(j,3)/figactive(i).nombrepoints;
        end
    elseif figactive(i).type==2
        x=figactive(i).centre(1);
        y=figactive(i).centre(2);
        z=figactive(i).centre(3);
    end
    text(x,y,z,num2str(figindex(i)));
end
xlabel('x');
ylabel('y');
zlabel('z');
alpha(.5);
grid ON
rotate3d ON    
set(handles.axes1,'DataAspectRatio',[1 1 1]);

% --------------------------------------------------------------------
function varargout = effacer(h, eventdata, handles, varargin)

if handles.figureindex~handles.nombrefigures
    for i=handles.figureindex:(handles.nombrefigures-1)
        handles.figure(i)=handles.figure(i+1);
    end
else
    handles.figureindex=handles.figureindex-1;
end

handles.figure(handles.nombrefigures)=[];
handles.nombrefigures=handles.nombrefigures-1;
afficher(h, eventdata, handles, varargin);

guidata(gcbo,handles);

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = calcul(h, eventdata, handles, varargin)



sb=5.670e-8; %Stefan-Boltzmann


nbpointsmax=50;%This is the number of sides by which we approximate the circles in the calculation. By increasing nbpointsmax,
               %we increase precision but we also increase calculation
               %time.

t0=clock;%To count the time required for the calculation.




%We determine which figures are active and we create a new structure with
%them.
nbfiguresactives=0;
for i=1:handles.nombrefigures
    if handles.figure(i).active==1
        nbfiguresactives=nbfiguresactives+1;
        figureactive(nbfiguresactives)=handles.figure(i);
    end
end



%We get the pertinent variables out of the structure.
% This part is because the rest of the function if from a script
% that has been copy-paste here.

for i=1:nbfiguresactives
    switch figureactive(i).type
    case 1
        %Number of points per figure.
        nbpoints(i)=figureactive(i).nombrepoints;
        %What do we know? Temperature? Radiative balance?
        connue(i)=figureactive(i).connue;
        if connue(i)==1
            %Temperature of the figure.
            T(i)=figureactive(i).temperature;
        elseif connue(i)==2
            %Radiative balance of the figure.
            q(i)=figureactive(i).bilanrad;
        end
        %Emissivity
        e(i)=figureactive(i).emissivite;
        %Coordinates of the vertices of each figure.
        for j=1:nbpoints(i);
            for composante=1:3
                coord(i,j,composante)=figureactive(i).coord(j,composante);
            end
        end
        
        
        %Filling the matrix to avoid holes when figure has less than the
        %maximum number of vertices.
        for j=1:nbpointsmax
            if j>nbpoints(i)
                coord(i,j,:)=coord(i,1,:);
            end
        end

    case 2
        %Number of vertices per figure.
        nbpoints(i)=nbpointsmax;
        %We know the temperature or the radiative balance?
        connue(i)=figureactive(i).connue;
        if connue(i)==1
            %Temperature of the surface
            T(i)=figureactive(i).temperature;
        elseif connue(i)==2
            %Radiative balance 
            q(i)=figureactive(i).bilanrad;
        end
        %Emissivity of the surface
        e(i)=figureactive(i).emissivite;
        
        nx=figureactive(i).normale(1);
        ny=figureactive(i).normale(2);
        nz=figureactive(i).normale(3);
        nx=nx/sqrt(nx^2+ny^2+nz^2);
        ny=ny/sqrt(nx^2+ny^2+nz^2);
        nz=nz/sqrt(nx^2+ny^2+nz^2);


        x0=figureactive(i).centre(1);
        y0=figureactive(i).centre(2);
        z0=figureactive(i).centre(3);
        
        R=figureactive(i).rayon;
        if nz==0
            if nx==0&ny==0
                msgbox('The three coordinates of the normal are 0!','ERROR','Error');
                break;
            elseif nx==0
                for j=1:nbpointsmax
                    theta=(j-1)/nbpointsmax*2*pi;
                    
                    xtemp(j)=R*cos(theta);
                    ytemp(j)=0;
                    ztemp(j)=R*sin(theta);
                    
                    coord(i,j,1)=xtemp(j)+x0;
                    coord(i,j,2)=ytemp(j)+y0;
                    coord(i,j,3)=ztemp(j)+z0;
                end
            elseif ny==0
                for j=1:nbpointsmax
                    theta=(j-1)/nbpointsmax*2*pi;
                    
                    xtemp(j)=0;
                    ytemp(j)=R*cos(theta);
                    ztemp(j)=R*sin(theta);
                    
                    coord(i,j,1)=xtemp(j)+x0;
                    coord(i,j,2)=ytemp(j)+y0;
                    coord(i,j,3)=ztemp(j)+z0;
                end
            else

                 for j=1:nbpointsmax
                    theta=(j-1)/nbpointsmax*2*pi;
                    
                    xtemp(j)=R*cos(theta)*sqrt(1/(1+(nx/ny)^2));
                    ytemp(j)=R*cos(theta)*sqrt(1/(1+(ny/nx)^2));
                    ztemp(j)=R*sin(theta);
                    
                    if nx/ny>0
                        xtemp(j)=-xtemp(j);
                    end
                    
                    coord(i,j,1)=xtemp(j)+x0;
                    coord(i,j,2)=ytemp(j)+y0;
                    coord(i,j,3)=ztemp(j)+z0;
                end
            end
        else
            nx=nx+1*10^(-10);
            ny=ny+1*10^(-10);
            nz=nz+1*10^(-10);
            
            theta0=atan(-nx/ny)+0.00001;

            for j=1:nbpointsmax/2
                theta=theta0+(j-1)/nbpointsmax*2*pi;
                xtemp(j)=-R*cos(theta)*nz/(cos(theta)*nx+sin(theta)*ny)/(nz^2/(cos(theta)*nx+sin(theta)*ny)^2+1)^(1/2);
                ytemp(j)=-R*sin(theta)*nz/(cos(theta)*nx+sin(theta)*ny)/(nz^2/(cos(theta)*nx+sin(theta)*ny)^2+1)^(1/2);
                ztemp(j)=R/(nz^2/(cos(theta)*nx+sin(theta)*ny)^2+1)^(1/2);
                
                xtemp(j+nbpointsmax/2)=-xtemp(j);
                ytemp(j+nbpointsmax/2)=-ytemp(j);
                ztemp(j+nbpointsmax/2)=-ztemp(j);
    
                coord(i,j,1)=xtemp(j)+x0;
                coord(i,j,2)=ytemp(j)+y0;
                coord(i,j,3)=ztemp(j)+z0;
            
                coord(i,j+nbpointsmax/2,1)=xtemp(j+nbpointsmax/2)+x0;
                coord(i,j+nbpointsmax/2,2)=ytemp(j+nbpointsmax/2)+y0;
                coord(i,j+nbpointsmax/2,3)=ztemp(j+nbpointsmax/2)+z0;
            end
        end
    end
end




set(gcf,'name',[handles.nomfenetre,': View factors calculation']);
%View factors calculation.
for i=1:nbfiguresactives
    F(i,i)=0;
    for j=1:nbfiguresactives
        if j>i
            set(gcf,'name',[get(gcf,'name'),'.']);
            pause(0);
             [F(i,j),F(j,i),Aire(i),Aire(j)]=viewfactor(transpose([coord(i,1:nbpoints(i),1);coord(i,1:nbpoints(i),2);coord(i,1:nbpoints(i),3)]),transpose([coord(j,1:nbpoints(j),1);coord(j,1:nbpoints(j),2);coord(j,1:nbpoints(j),3)]),6);
         end
    end
end


for i=1:nbfiguresactives
    if e(i)==1
        e(i)=0.999999;%Code added to avoid a division by zero.
    end
    E(i)=((1-e(i))/e(i)); 
    if connue(i)==1
        C(i)=sb*T(i).^4;
    elseif connue(i)==2
        C(i)=q(i);
    end
end
C=transpose(C);
 	

for i=1:nbfiguresactives,
     for j=1:nbfiguresactives
        if connue(i)==1   
            c=0;    
              if i~=j
                    A(i,j)=-F(i,j)*E(i); 
              else % i==j
                    for k=1:nbfiguresactives
                        c=c+F(i,k);
                    end %for
                    A(i,j)=c*E(i)+1; 
              end  
          elseif connue(i)==2
              c=0;    
              if i~=j
                    A(i,j)=(-Aire(i)*F(i,j));
              else % i==j
                    for k=1:nbfiguresactives
                        c=c+F(i,k);
                    end %for
                    A(i,j)=c*Aire(i); 
              end  
         end
   end
   j=1;
end

J=A\C;

for i=1:nbfiguresactives
   	if connue(i)==1
        
        if e(i)~=1   
             qfinal(i)=(C(i)-J(i))/(E(i)*(1/Aire(i)));
         else 
              D=0;
             for k=1:nbfiguresactives
                 D=D+F(i,j)*sb*(T(i)^4-T(k)^4);
             end 
             qfinal(i)=Aire(i)*D;
       	end
    elseif connue(i)==2
        qfinal(i)=q(i);
        if q(i)==0
           T(i)=(J(i)/sb)^0.25;
       elseif e(i)~=1
           T(i)=(q(i)*E(i)*(1/Aire(i))+J(i)/sb)^0.25;
       elseif e(i)==1
           T(i)=(J(i)/sb)^0.25;
       end
    end
end


set(gcf,'name',[handles.nomfenetre,': Finished. Time required: ',num2str(etime(clock,t0)),' seconds']);

%Creation of the web page containing the results.
fichier=fopen('results.html','w');
%head
fprintf(fichier,'<html>\n\n<head>\n<meta http-equiv="Content-Language" content="fr-ca">\n<meta name="GENERATOR" content="Heat transfers by radiation">\n<meta name="ProgId" content="FrontPage.Editor.Document">\n<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">\n<title>Results of the calculation</title>\n</head>\n');

fprintf(fichier,'<body>\n');

%table at the top of the page
fprintf(fichier,'<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%%" id="AutoNumber1">\n  <tr>\n    <td width="100%%" colspan="2">\n    <p align="center"><b><font size="5">Results of calculation - View factors and energy \n    balance</font></b></td>\n  </tr>\n');


fprintf(fichier,' <tr>\n    <td width="30%%">\n    <p align="center"><b><span style="background-color: #FFFFFF">');

for i=1:nbfiguresactives
    for j=1:nbfiguresactives
        if i~=j
            fprintf(fichier,'f(%d-&gt;%d)&nbsp;&nbsp;&nbsp; %g<br>\n',i,j,F(i,j));
        end
        
    end
end

fprintf(fichier,'</b></td>\n    <td width="70%%">\n    <p align="center"><b>');

for i=1:nbfiguresactives
    fprintf(fichier,'%s: Area = %g m^2 | T = %g K | Q = %g W <br>\n',[num2str(i),': ',figureactive(i).nom],Aire(i),T(i),qfinal(i));
end

fprintf(fichier,'</b></td>\n  </tr>\n');

%table (bottom)
fprintf(fichier,'<tr>\n    <td width="100%%" colspan="2">\n    <p align="center"><font size="4">Calculated: %s <br>\n Programmed by Nicolas Lauzier <br>\n with the collaboration of Daniel Rousse <br>\n Université Laval - 2004 </font></td>\n  </tr>\n</table>',datestr(clock,0));

fprintf(fichier,'\n\n</body>\n\n</html>');

fclose(fichier);

%Open the web page with explorer.
!explorer results.html



% --------------------------------------------------------------------
function varargout = enregistrer(h, eventdata, handles, varargin)

%This function is to save the input data in a .sav file.

[nomfichier,chemin] = uiputfile({'*.sav','Input data files (*.sav)'},'Save as...');
if nomfichier==0
    return
end
if nomfichier((length(nomfichier)-3):(length(nomfichier)))~='.sav'
    nomfichier=[nomfichier,'.sav'];
end

fichier=fopen([chemin,nomfichier],'w');

%Number of figures
fprintf(fichier,'%d\n\n',handles.nombrefigures);

%Loop to save information about each figure.

for i=1:handles.nombrefigures
    
    %This is to avoid errors when opening figure names containing space
    for j=1:length(handles.figure(i).nom)
        if handles.figure(i).nom(j)==' '
            nomtemporaire(j)='_';
        else
            nomtemporaire(j)=handles.figure(i).nom(j);
        end
    end
    fprintf(fichier,'%s\n',nomtemporaire);
    clear nomtemporaire;
    fprintf(fichier,'%d\n',handles.figure(i).connue);
    if handles.figure(i).emissivite~''
        fprintf(fichier,'%g\n',handles.figure(i).emissivite);
    else
        fprintf(fichier,'-1\n');
    end
    fprintf(fichier,'%d\n',handles.figure(i).active);
    fprintf(fichier,'%d\n',handles.figure(i).nombrepoints);
    
    if handles.figure(i).temperature~''
        fprintf(fichier,'%g\n',handles.figure(i).temperature);
    else
        fprintf(fichier,'-1\n');
    end
    
    if handles.figure(i).bilanrad~''
        fprintf(fichier,'%g\n',handles.figure(i).bilanrad);
    else
        fprintf(fichier,'-1\n');
    end

    for j=1:10
        fprintf(fichier,'%g %g %g\n',handles.figure(i).coord(j,1),handles.figure(i).coord(j,2),handles.figure(i).coord(j,3));
    end
    
    fprintf(fichier,'%d\n',handles.figure(i).type);
    
    fprintf(fichier,'%g %g %g\n',handles.figure(i).centre);
    fprintf(fichier,'%g %g %g\n',handles.figure(i).normale);
    fprintf(fichier,'%g\n',handles.figure(i).rayon);
    
    fprintf(fichier,'\n\n');
    
    
end

fclose(fichier);

% --------------------------------------------------------------------
function varargout = ouvrir(h, eventdata, handles, varargin)

%This function is to open input data file previously saved.

clear handles.figure;
[nomfichier,chemin] = uigetfile({'*.sav','Input data files (*.sav)'},'Open file...');

fichier=fopen([chemin,nomfichier]);

handles.nombrefigures=fscanf(fichier,'%g',1);

for i=1:handles.nombrefigures
    nomtemporaire=fscanf(fichier,'%s',1);
    for j=1:length(nomtemporaire)
        if nomtemporaire(j)=='_'
            nomtemporaire(j)=' ';
        end
    end
    handles.figure(i).nom=nomtemporaire;
    handles.figure(i).connue=fscanf(fichier,'%d',1);
    a=fscanf(fichier,'%g',1);
    if a~-1
        handles.figure(i).emissivite=a;
    else
        handles.figure(i).emissivite='';
    end
    handles.figure(i).active=fscanf(fichier,'%d',1);
    handles.figure(i).nombrepoints=fscanf(fichier,'%d',1);
    
    a=fscanf(fichier,'%g',1);
    if a~-1
        handles.figure(i).temperature=a;
    else
        handles.figure(i).temperature='';
    end
    
    a=fscanf(fichier,'%g',1);
    if a~-1
        handles.figure(i).bilanrad=a;
    else
        handles.figure(i).bilanrad='';
    end


    for j=1:10
        handles.figure(i).coord(j,1)=fscanf(fichier,'%g',1);
        handles.figure(i).coord(j,2)=fscanf(fichier,'%g',1);
        handles.figure(i).coord(j,3)=fscanf(fichier,'%g',1);
    end


    handles.figure(i).type=fscanf(fichier,'%d',1);
    
    handles.figure(i).centre(1)=fscanf(fichier,'%g',1);
    handles.figure(i).centre(2)=fscanf(fichier,'%g',1);
    handles.figure(i).centre(3)=fscanf(fichier,'%g',1);
            
    handles.figure(i).normale(1)=fscanf(fichier,'%g',1);
    handles.figure(i).normale(2)=fscanf(fichier,'%g',1);
    handles.figure(i).normale(3)=fscanf(fichier,'%g',1);
    
    handles.figure(i).rayon=fscanf(fichier,'%g',1);
    
end

handles.figureindex=1;
guidata(gcbo,handles);

graphique(h, eventdata, handles, varargin);
set(handles.lstfigures,'value',1);
afficher(h, eventdata, handles, varargin);



% --------------------------------------------------------------------
function varargout = Fichier_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = nouveau_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = ouvrir_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = enregistrer_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = quitter_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = menuaide_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = aide_Callback(h, eventdata, handles, varargin)

!notepad radtransfer (GUI) - readme.txt


% --------------------------------------------------------------------
function varargout = apropos_Callback(h, eventdata, handles, varargin)

OPENFIG('about.fig', 'reuse');




% --------------------------------------------------------------------
function varargout = txttemperature_Callback(h, eventdata, handles, varargin)
actualiser(h, eventdata, handles, varargin);

%---------------------------------------------------------------------
function varargout = ecriturerouge(h, eventdata, handles, varargin)
actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = ppmtype_Callback(h, eventdata, handles, varargin)

switch get(handles.ppmtype,'value')
case 1
    handles.figure(handles.figureindex).type=1;
    visibilite(h, eventdata, handles,varargin);
case 2
    handles.figure(handles.figureindex).type=2;
    visibilite(h, eventdata, handles,varargin);
end
guidata(gcbo,handles);

% --------------------------------------------------------------------
function varargout = visibilite(h, eventdata, handles, varargin)

switch handles.figure(handles.figureindex).type
case 1
    set(handles.lblX,'visible','on');
    set(handles.lblY,'visible','on');
    set(handles.lblZ,'visible','on');
    set(handles.txtX1,'visible','on');
    set(handles.txtY1,'visible','on');
    set(handles.txtZ1,'visible','on');
    set(handles.txtX2,'visible','on');
    set(handles.txtY2,'visible','on');
    set(handles.txtZ2,'visible','on');
    set(handles.txtX3,'visible','on');
    set(handles.txtY3,'visible','on');
    set(handles.txtZ3,'visible','on');
    set(handles.txtX4,'visible','on');
    set(handles.txtY4,'visible','on');
    set(handles.txtZ4,'visible','on');
    set(handles.txtX5,'visible','on');
    set(handles.txtY5,'visible','on');
    set(handles.txtZ5,'visible','on');
    set(handles.txtX6,'visible','on');
    set(handles.txtY6,'visible','on');
    set(handles.txtZ6,'visible','on');
    set(handles.txtX7,'visible','on');
    set(handles.txtY7,'visible','on');
    set(handles.txtZ7,'visible','on');
    set(handles.txtX8,'visible','on');
    set(handles.txtY8,'visible','on');
    set(handles.txtZ8,'visible','on');
    set(handles.txtX9,'visible','on');
    set(handles.txtY9,'visible','on');
    set(handles.txtZ9,'visible','on');
    
    set(handles.lblcentre,'visible','off');
    set(handles.txtXcentre,'visible','off');
    set(handles.txtYcentre,'visible','off');
    set(handles.txtZcentre,'visible','off');
    set(handles.lblXcentre,'visible','off');
    set(handles.lblYcentre,'visible','off');
    set(handles.lblZcentre,'visible','off');
    
    set(handles.lblnormale,'visible','off');
    set(handles.txtXnormale,'visible','off');
    set(handles.txtYnormale,'visible','off');
    set(handles.txtZnormale,'visible','off');
    set(handles.lblXnormale,'visible','off');
    set(handles.lblYnormale,'visible','off');
    set(handles.lblZnormale,'visible','off');
    
    set(handles.lblrayon,'visible','off');
    set(handles.txtrayon,'visible','off');
    
case 2
    set(handles.lblX,'visible','off');
    set(handles.lblY,'visible','off');
    set(handles.lblZ,'visible','off');
    set(handles.txtX1,'visible','off');
    set(handles.txtY1,'visible','off');
    set(handles.txtZ1,'visible','off');
    set(handles.txtX2,'visible','off');
    set(handles.txtY2,'visible','off');
    set(handles.txtZ2,'visible','off');
    set(handles.txtX3,'visible','off');
    set(handles.txtY3,'visible','off');
    set(handles.txtZ3,'visible','off');
    set(handles.txtX4,'visible','off');
    set(handles.txtY4,'visible','off');
    set(handles.txtZ4,'visible','off');
    set(handles.txtX5,'visible','off');
    set(handles.txtY5,'visible','off');
    set(handles.txtZ5,'visible','off');
    set(handles.txtX6,'visible','off');
    set(handles.txtY6,'visible','off');
    set(handles.txtZ6,'visible','off');
    set(handles.txtX7,'visible','off');
    set(handles.txtY7,'visible','off');
    set(handles.txtZ7,'visible','off');
    set(handles.txtX8,'visible','off');
    set(handles.txtY8,'visible','off');
    set(handles.txtZ8,'visible','off');
    set(handles.txtX9,'visible','off');
    set(handles.txtY9,'visible','off');
    set(handles.txtZ9,'visible','off');
    
    set(handles.lblcentre,'visible','on');
    set(handles.txtXcentre,'visible','on');
    set(handles.txtYcentre,'visible','on');
    set(handles.txtZcentre,'visible','on');
    set(handles.lblXcentre,'visible','on');
    set(handles.lblYcentre,'visible','on');
    set(handles.lblZcentre,'visible','on');
    
    set(handles.lblnormale,'visible','on');
    set(handles.txtXnormale,'visible','on');
    set(handles.txtYnormale,'visible','on');
    set(handles.txtZnormale,'visible','on');
    set(handles.lblXnormale,'visible','on');
    set(handles.lblYnormale,'visible','on');
    set(handles.lblZnormale,'visible','on');
    
    set(handles.lblrayon,'visible','on');
    set(handles.txtrayon,'visible','on');
  
end




% --------------------------------------------------------------------
function varargout = txtXcentre_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = txtYcentre_Callback(h, eventdata, handles, varargin)


actualiser(h, eventdata, handles, varargin);

% --------------------------------------------------------------------
function varargout = txtZcentre_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = txtXnormale_Callback(h, eventdata, handles, varargin)


actualiser(h, eventdata, handles, varargin);

% --------------------------------------------------------------------
function varargout = txtYnormale_Callback(h, eventdata, handles, varargin)


actualiser(h, eventdata, handles, varargin);

% --------------------------------------------------------------------
function varargout = txtZnormale_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = txtrayon_Callback(h, eventdata, handles, varargin)

actualiser(h, eventdata, handles, varargin);


% --------------------------------------------------------------------
function varargout = mnufigure_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = mnugraphique_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = mnuactualiser_Callback(h, eventdata, handles, varargin)




% --------------------------------------------------------------------
function varargout = Untitled_3_Callback(h, eventdata, handles, varargin)


