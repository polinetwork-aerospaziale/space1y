View factors and radiation heat transfers

Program written by Nicolas Lauzier in collaboration with Daniel Rousse, Université Laval, 2003-2004
-----------------------------

To use the program, you need:

about.fig
about.m
radtransfer.m
radtransfer.fig
viewfactor.m
functionintegral.m
functionviewfactorarea.m

and the example input data file:

demo.sav

------------------------------

rad transfer is a matlab program that calculates view factors and radiative heat transfers between
two or many figures. By knowing the geometry (coordinates of the vertices defining the outline for 
a polygon or the radius, the normal and the coordinates of the center for a circle), the emissivity
of the surface and either the temperature of the surface or the radiative balance, you can calculate
each view factor and each missing variable (temperature or radiative balance, the unknown one for each
figure). 

rad transfer is using the CDIF method for calculating view factors.

It is important to avoid certain situations that will results in errors:
   - The geometry of the surfaces can't interfere.
   - The geometry of the surfaces can't cross a plane containing another surfaces. (A surface can't be
     in front AND behind another surfaces, this would result in an error in view factor calculation)
   - Avoid impossible situation, such as emissivity < 0 or > 1, negative temperature or any other parameter
     with no physical sense.
   - Each vertice defining the outline of the figure must be on the same plane. (only 2D surfaces, no volume)

Note: When calculating view factors, the circles are approximated by polygons with 50 sides, which results
      in a good relative precision.
------------------------------

To use the program, simply type "radtransfer" in the command window when you are in a directory containing
the required files. This will launch the GUI (graphical user interface).

Enter the parameters of each figure. You can add or remove any amount of figures by clicking "Add a figure" or "Erase current figure".

For each figure you must:

   - Indicate if the figure is either a polygon or a circle.

   - For a polygon, you must:
       - Enter the coordinates of the vertices defining the outline of the figure.

   - For a circle, you must:
       - Enter the coordinates of the center of the circle.
       - Enter a vector which is normal to the plane containing the circle.
       - Enter the radius of the circle.

   - Indicate the emissivity of the surface.

   - Indicate if the figure is active or not: a desactivated figure will not be part of the calculs
     and will not appear on the graphic. [A] or [D] appear to the right of the figure name in the list
     of figure, indicating if the figure is activated or desactivated.

   - Indicate which variable is known and which is unknown: the temperature of the surface (in K) or 
     its radiative balance. You must enter the value of the know variable. When you will launch the
     calculation procedure, the program will calculates the value of the unknown variable. 
   
   - You can also enter a name for each figure. This is only useful if you have a lot of figures
     and if you save your list of figure.

After every parameters are entered, you just click on the "Calculate" button to launch the calculation
procedure. The program will then create an HTML file containing the results and will open it using
internet explorer. The HTML file is name results.html, you can save it on another name with the 
explorer.

Some other options may be useful:

- You can save and re-open later your list of figures (.sav files). This can be useful when you want
  to modify or enter other figures later.

- You can save the graphic in .JPG format. This may be useful if you are keeping your results and want
  to have a visual representation of the situation.

   
--------------------------------
This matlab program is available on www.mathworks.com

Questions/comments: nlauzier@yahoo.com