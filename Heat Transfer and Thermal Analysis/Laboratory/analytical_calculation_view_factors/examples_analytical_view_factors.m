%Author: M. Guilizzoni, Department of Energy, Politecnico di Milano
%March 2013
%
%example: [F12,F12_2D]=examples_analytical_view_factors(1,[1 1 1]);
%[        [F12,F12_2D]=examples_analytical_view_factors(1,[1 1 1]);
%
function [F12,F12_2D]=examples_analytical_view_factors(example,scale_factors)

%scale factors for dimensions / distance
scale_x=scale_factors(1);
scale_y=scale_factors(2);
scale_z=scale_factors(3);

%to choose the example to analyse
switch(example)

%------------------------------------------------------------------------%
case(1)
%EXAMPLE 1 (example 1 Tab. 10.3)

%basic rectangles, unit dimension along all directions
rect1(1,:)=[0 0 0];
rect1(2,:)=[1 0 0];
rect1(3,:)=[1 1 0];
rect1(4,:)=[0 1 0];

rect2(1,:)=[0 0 1];
rect2(2,:)=[1 0 1];
rect2(3,:)=[1 1 1];
rect2(4,:)=[0 1 1];

rect1(:,1)=rect1(:,1)*scale_x;
rect1(:,2)=rect1(:,2)*scale_y;
rect1(:,3)=rect1(:,3)*scale_z;

rect2(:,1)=rect2(:,1)*scale_x;
rect2(:,2)=rect2(:,2)*scale_y;
rect2(:,3)=rect2(:,3)*scale_z;

%3D view factor calculation
F12=calculate_view_factors_by_equation(rect1,rect2,example);

%2D approximation (infinite along y) (example 1 Tab. 10.2)
w=rect1(2,1)-rect1(1,1);
h=rect2(1,3)-rect1(1,3);
F12_2D=(1+(h/w)^2)^0.5-(h/w);

%------------------------------------------------------------------------%

case(2)
%EXAMPLE 2 (example 2 Tab. 10.3)

%basic rectangles, unit dimension along all directions
rect1(1,:)=[0 0 0];
rect1(2,:)=[1 0 0];
rect1(3,:)=[1 1 0];
rect1(4,:)=[0 1 0];

rect2(1,:)=[0 0 0];
rect2(2,:)=[0 1 0];
rect2(3,:)=[0 1 1];
rect2(4,:)=[0 0 1];

rect1(:,1)=rect1(:,1)*scale_x;
rect1(:,2)=rect1(:,2)*scale_y;
rect1(:,3)=rect1(:,3)*scale_z;

rect2(:,1)=rect2(:,1)*scale_x;
rect2(:,2)=rect2(:,2)*scale_y;
rect2(:,3)=rect2(:,3)*scale_z;

%3D view factor calculation
F12=calculate_view_factors_by_equation(rect1,rect2,example);

%2D approximation (infinite along y) (example 3 Tab. 10.2)
h=rect2(4,3)-rect1(1,3);
w=rect1(2,1)-rect1(1,1);
F12_2D=1/2*(1+(h/w)-(1+(h/w)^2)^0.5);

%------------------------------------------------------------------------%
end

%visualization of the two rectangles

figure;
hold on;

triangle11=[rect1(1,:);rect1(2,:);rect1(3,:)];
triangle12=[rect1(1,:);rect1(3,:);rect1(4,:)];

triangle21=[rect2(1,:);rect2(2,:);rect2(3,:)];
triangle22=[rect2(1,:);rect2(3,:);rect2(4,:)];

trisurf([1 2 3],triangle11(:,1),triangle11(:,2),triangle11(:,3),'FaceColor','cyan','EdgeColor','none');
trisurf([1 2 3],triangle12(:,1),triangle12(:,2),triangle12(:,3),'FaceColor','cyan','EdgeColor','none');

trisurf([1 2 3],triangle21(:,1),triangle21(:,2),triangle21(:,3),'FaceColor','red','EdgeColor','none');
trisurf([1 2 3],triangle22(:,1),triangle22(:,2),triangle22(:,3),'FaceColor','red','EdgeColor','none');

view(60,30);
alpha(0.65);
axis equal;
xlabel('x');
ylabel('y');
zlabel('z');
grid on;

end
        