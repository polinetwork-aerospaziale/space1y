          % UTILIZZO DELLE DIFFERENZE FINITE PER LA SOLUZIONE DI UN PROBLEMA DI %
  % CONDUZIONE BIDIMENSIONALE, in condizioni stazionarie e senza generazione di potenza %
% LASTRA PIANA CON TEMPERATURA SUL CONTORNO IMPOSTA (condizioni al contorno di 1a specie) %

T1=150;              % temperatura sul contorno 1
T2=20;               % temperatura sul contorno 2
T3=20;               % temperatura sul contorno 3
T4=20;               % temperatura sul contorno 4
m=50;                % numero di nodi in cui si discretizza lungo l'asse x (contorni 2 e 4)
n=50;                % numero di nodi in cui si discretizza lungo l'asse y (contorni 1 e 3)
f=m*n;               % numero totale di nodi
X=10;                % lunghezza della lastra lungo l'asse x
Y=10;                % lunghezza della lastra lungo l'asse y
dx=X/(m-1);          % spaziatura tra i nodi lungo l'asse x
dy=Y/(n-1);          % spaziatura tra i nodi lungo l'asse y
b=(dx*dx)/(dy*dy);   % fattore di spaziatura reticolare
it=0;                % iterazioni necessarie perch� la soluzione converga
k=40;                % conducibilit� del materiale di cui � fatta la lastra

DT_soglia=0.01;   % inizializzazione del valore soglia di DT sotto il quale la
                  % soluzione viene ritenuta accettabile
Max_DT=100;       % inizializzazione del valore massimo di DT rilevato al termine di
                  % ogni iterazione

T=zeros(m,n);     % inizializzazione della matrice delle T nei nodi
DT=zeros(m,n);    % inizializzazione della matrice delle differenze di T nei nodi
                  % tra due iterazioni consecutive durante la soluzione

% assegnazione delle temperature ai nodi appartenenti ai contorni
for i=2:m-1   T(i,1)=T1;   end
for i=1:n     T(m,i)=T2;   end
for i=2:m-1   T(i,n)=T3;   end
for i=1:n     T(1,i)=T4;   end

% assegnazione di un valore di primo tentativo ai nodi interni alla lastra
T0=(T1+T2+T3+T4)/4;
for i=2:m-1
    for j=2:n-1 T(i,j)=T0; end
end

% soluzione del problema: calcolo delle T nei nodi interni, iterato finch� la soluzione
% non risulta convergente
while Max_DT>DT_soglia
      for i=2:m-1
          for j=2:n-1 Tt(i,j)=(T(i+1,j)+T(i-1,j)+b*T(i,j+1)+b*T(i,j-1))/(2+2*b);
		      % Tt(i,j)=0.25*(T(i,j-1)+T(i,j+1)+T(i-1,j)+T(i+1,j));   % se b=1!
                      DT(i,j)=abs(Tt(i,j)-T(i,j));
                      T(i,j)=Tt(i,j);
          end
      end
      Max_DT=max(max(DT));
      it=it+1;
end

it   % visualizza il numero totale di iterazioni che sono state necessarie per avere 
     % la convergenza della soluzione

% visualizzazione 2D e 3D dei risultati (campo di temperatura nella lastra)
x=0:dx:X;
y=0:dy:Y;

% 2D
figure;
surf(x,y,T);
view(2);  % vista 2D di default
%colorbar('vert')
axis image;   % stessa scala per x e y, grafico adattato esattamente all'immagine
title('Distribuzione della temperatura nella lastra piana - vista 2D')
xlabel('x')
ylabel('y')
zlabel('Temperatura [�C]')

% 3D
figure;
surf(x,y,T);
view(45,45);
% view(3); % vista 3D di default
%colorbar('vert')
title('Distribuzione della temperatura nella lastra piana - vista 3D')
xlabel('x')
ylabel('y')
zlabel('Temperatura [�C]')

% visualizzazione delle isoterme e dei vettori flusso termico
figure;
[qx,qy]=gradient(T,dx,dy);
qx=-k*qx;
qy=-k*qy;
contour(x,y,T);         % disegna le isoterme
hold on;                % sovrappone le immagini nella stessa finestra figura
quiver(x,y,qx,qy,10);   % disegna i vettori flusso termico, adattandoli all'immagine e riscalandoli *10
axis equal;             % stessa scala per i due assi e y
axis([0 X 0 Y]);        % limiti della finestra di visualizzazione lungo i due assi
%colorbar('vert')
title('Isoterme e vettori flusso termico nella lastra piana - vista 2D')
xlabel('x')
ylabel('y')
hold off;