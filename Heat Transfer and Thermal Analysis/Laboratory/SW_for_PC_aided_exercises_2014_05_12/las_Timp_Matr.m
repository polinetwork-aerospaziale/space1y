% UTILIZZO DELLE DIFFERENZE FINITE PER LA SOLUZIONE DI UN PROBLEMA DI %
% CONDUZIONE BIDIMENSIONALE, in condizioni stazionarie e senza generazione di potenza %
% LASTRA PIANA CON TEMPERATURA SUL CONTORNO IMPOSTA (condizioni al contorno di 1a specie) %

% UTILIZZA IL METODO DI INVERSIONE DIRETTA DELLA MATRICE DEI COEFFICIENTI %
% INVECE DELLA SOLUZIONE ITERATIVA %

T1=150;              % temperatura sul contorno 1
T2=20;               % temperatura sul contorno 2
T3=20;               % temperatura sul contorno 3
T4=20;               % temperatura sul contorno 4
m=50;                % numero di nodi in cui si discretizza lungo l'asse x (contorni 2 e 4)
n=50;                % numero di nodi in cui si discretizza lungo l'asse y (contorni 1 e 3)
f=m*n;               % numero totale di nodi
X=10;                % lunghezza della lastra lungo l'asse x
Y=10;                % lunghezza della lastra lungo l'asse y
dx=X/(m-1);          % spaziatura tra i nodi lungo l'asse x
dy=Y/(n-1);          % spaziatura tra i nodi lungo l'asse y
b=(dx*dx)/(dy*dy);   % fattore di spaziatura reticolare
it=0;                % iterazioni necessarie perch� la soluzione converga
k=40;                % conducibilit� del materiale di cui � fatta la lastra

% Costruzione della matrice dei coefficienti

a=zeros(f,f);

% Nodi con temperatura imposta
for j=m+1:m:f-2*m+1
   a(j,j)=1;
end

for j=1:(m-1)
   a(j,j)=1;
end

for j=m:m:f
    a(j,j)=1;
end

for j=(f-m+1):(f-1)
   a(j,j)=1;
end

% Nodi interni
for p=1:(n-2)
   for j=(p*m+2):((p+1)*m-1)
      a(j,j-m)=-b;
      a(j,j+m)=-b;
      a(j,j-1)=-1;
      a(j,j+1)=-1;
      a(j,j)=2*(1+b);
   end
end


% Costruzione del vettore dei termini noti

c=zeros(f,1);

% Nodi con temperatura imposta
for j=m+1:m:f-2*m+1
   c(j)=T1;
end

for j=1:(m-1)
   c(j)=T2;
end

for j=m:m:f
    c(j)=T3;
end

for j=(f-m+1):(f-1)
   c(j)=T4;
end

% Risoluzione del sistema

a1=inv(a);
t=a1*c;

% Risultati finali e grafici

x=0:dx:X;
y=0:dy:Y;
[X,Y]=meshgrid(x,y);
T=zeros(n,m);
for v=1:n
   for w=1:m
      T(v,w)=t((n-v)*m+w);
   end
end

% 3D
figure(1)
surf(x,y,T)
view(45,45)
shading interp
title('Distribuzione di temperatura nella lastra - vista 3D')
xlabel('x [m]')
ylabel('y [m]')
zlabel('T [�C]')

% 2D
figure(2)
surf(x,y,T)
view(2)
shading interp
title('Distribuzione di temperatura nella lastra - vista 2D')
xlabel('x [m]')
ylabel('y [m]')

% visualizzazione delle isoterme e dei vettori flusso termico
figure(3);
[qx,qy]=gradient(T,dx,dy);
qx=-k*qx;
qy=-k*qy;
contour(x,y,T);         % disegna le isoterme
hold on;                % sovrappone le immagini nella stessa finestra figura
quiver(x,y,qx,qy,10);   % disegna i vettori flusso termico, adattandoli all'immagine e riscalandoli *10
axis equal;             % stessa scala per i due assi e y
%axis([0 X 0 Y]);        % limiti della finestra di visualizzazione lungo i due assi
%colorbar('vert')
title('Isoterme e vettori flusso termico nella lastra piana - vista 2D')
xlabel('x')
ylabel('y')
hold off;